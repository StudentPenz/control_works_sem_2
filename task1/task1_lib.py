def rectangle_rule(func, a, b, nseg):
    dx = 1.0 * (b - a) / nseg
    sum = 0.0
    xstart = a + dx
    for i in range(nseg):
        sum += func(xstart + i * dx)

    return sum * dx


def trapezoid(func, a, b, nseg):
    dx = 1.0 * (b - a) / nseg
    sum = 0.5 * (func(a) + func(b))
    for i in range(1, nseg):
        sum += func(a + i * dx)

    return sum * dx


def simpson(f, a, b, n):
    h = (b-a)/n
    k = 0.0
    x = a + h
    for i in range(1, n//2 + 1):
        k += 4*f(x)
        x += 2*h
    x = a + 2*h
    for i in range(1, n//2):
        k += 2*f(x)
        x += 2*h
    return (h/3)*(f(a)+f(b)+k)
