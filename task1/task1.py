import math
import task1_lib

a = 10
x = 10


def g(x):
    return 9 * (7 * a**2 + 39 * a * x + 20 * x**2)/(9 * a**2 + 59 * a * x + 30 * x**2)


print('rectangle', task1_lib.rectangle_rule(g, 1, 10, 5))
print('trapezoid', task1_lib.trapezoid(g, 1, 10, 5))
print('simpson', task1_lib.simpson(g, 1, 10, 5))
