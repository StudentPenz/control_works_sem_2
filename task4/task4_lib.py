from numpy import exp, linspace, convolve
import statistics
import copy
import collections


def median_filter(data, window_size):
    window = []
    output = []
    for elem in data:
        window.append(elem)
        if len(window) > window_size:
            temp = []
            for i in range(len(window)):
                if i == 0:
                    pass
                else:
                    temp.append(window[i])
            window = temp
        if len(window) == 0:
            output = 0
        else:
            output.append(statistics.median(window))
    return output


def sma(data, window):
    lst = copy.copy(data)
    queue = collections.deque()
    for i in range(len(lst)):
        queue.append(lst[i])
        queue_length = len(queue)
        if queue_length > window:
            queue.popleft()
            queue_length -= 1
        if queue_length == 0:
            lst[i] = 0
        else:
            lst[i] = sum(queue) / queue_length
    return lst


def ema(data, window):
    lst = copy.copy(data)
    queue = collections.deque()
    for i in range(len(lst)):
        queue_length = len(queue)
        if queue_length == 0:
            queue.append(lst[i])
            pass
        elif queue_length > window:
            queue.popleft()
        alpha = 2 / (window + 1)
        lst[i] = (alpha * lst[i]) + ((1 - alpha) * queue[-1])
        queue.append(lst[i])
    return lst

