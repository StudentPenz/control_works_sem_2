import xlrd
import task4_lib
import matplotlib.pyplot as plt
import matplotlib
import PyQt5
matplotlib.use('Qt5Agg')


file = xlrd.open_workbook('data.xls', formatting_info=True)
sheet = file.sheet_by_index(0)
data = sheet.col_values(22, start_rowx=1, end_rowx=2502)
data = [float(elem) for elem in data]

print(data)
print(len(data))


mf = task4_lib.median_filter(data, 25)
sma = task4_lib.sma(data, 25)
ema = task4_lib.ema(data, 25)
size = [i for i in range(len(data))]


plt.title('График отфильтрованных и изначальных данных')
plt.xlabel('Номер элемента')
plt.ylabel('Значение')
plt.plot(size, data)
plt.plot(size, sma, 'red', label="SMA")
plt.plot(size, mf, 'green', label="MF")
plt.plot(size, ema, 'blue', label="EMA")
#plt.xlim(0, 500)
plt.legend()
plt.show()
