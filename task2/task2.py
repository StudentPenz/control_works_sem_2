from task1 import task1_lib
import matplotlib.pyplot as plt

a = 0.1
x = 0.2

integrated_values = ([], [], [], [], [])
inter = []


def g(x):
    return 9 * (7 * a**2 + 39 * a * x + 20 * x**2)/(9 * a**2 + 59 * a * x + 30 * x**2)


for i in range(10, 100, 10):
    integrated_values[0].append(task1_lib.rectangle_rule(g, 1, 10, i))
    integrated_values[1].append(task1_lib.trapezoid(g, 1, 10, i))
    integrated_values[2].append(task1_lib.simpson(g, 1, 10, i))
    inter.append(i)

for i in range(3):
    for j in range(len(integrated_values[i])):
        integrated_values[i][j] = (-0.25 - integrated_values[i][j])


plt.plot(inter, integrated_values[0], label="RectanglesRule")
plt.plot(inter, integrated_values[1], 'y', label="TrapeziumRule")
plt.plot(inter, integrated_values[2], 'm', label="SimpsonRule")
plt.xlabel("Number of split points")
plt.ylabel("Absolute accuracy")
plt.legend()
plt.show()
